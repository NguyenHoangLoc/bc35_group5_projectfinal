import logo from "./logo.svg";
import "./App.css";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import AdminJobPage from "./Pages/AdminPage/AdminJobPage/AdminJobPage";
import AdminJobTypePage from "./Pages/AdminPage/AdminJobTypePage/AdminJobTypePage";
import AdminServicePage from "./Pages/AdminPage/AdminServicePage/AdminServicePage";
import AdminPage from "./Pages/AdminPage/AdminPage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import HeaderAdminPage from "./HOC/HeaderAdminPage/HeaderAdminPage";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./HOC/Layout";
import JobTitle from "./Pages/Jobtitle/Jobtitle";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />

          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/register" element={<RegisterPage />} />
          <Route
            path="/title/:id"
            element={
              <Layout>
                <JobTitle />
              </Layout>
            }
          />
          <Route path="/admin/job" element={<AdminJobPage />}></Route>
          <Route path="/admin/jobtype" element={<AdminJobTypePage />}></Route>

          <Route path="/admin/service" element={<AdminServicePage />}></Route>
          <Route
            path="/admin"
            element={
              <HeaderAdminPage>
                <AdminPage />
              </HeaderAdminPage>
            }
          ></Route>
        </Routes>
        <ToastContainer />
      </BrowserRouter>
    </div>
  );
}

export default App;
