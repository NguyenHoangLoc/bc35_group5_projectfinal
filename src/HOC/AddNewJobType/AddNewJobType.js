import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Grid, Input, TextField } from "@mui/material";
import { useFormik } from "formik";
import { adminService } from "../../services/adminService";
import _ from "lodash";
import { useState } from "react";
import { toast } from "react-toastify";
import { withAuth } from "../../services/configURL";
import { useSelector } from "react-redux";
export default function AddJobType() {
  const[jobType, setJobType]=useState({})
  const [open, setOpen] = React.useState(false);
  const { token } = useSelector((state) => {
    return state.userSlice.user;
  });
  withAuth(token);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //
  const frm = useFormik({
    initialValues: {
      id: 0,
      tenLoaiCongViec: "",
    },
    onSubmit: (values) => {
        adminService
        .postJobType(values)
        .then((res) => {
          handleClose();
          toast.success("Add service successfully");
          setJobType([res.data, ...jobType]);
        })
        .catch((err) => {
        //   toast.error("Add service failed");
        });
    },
  });

  return (
    <div className="mb-3">
      <Button variant="outlined" onClick={handleClickOpen}>
        ADD New Job type
      </Button>
      <Dialog
        className="dialog_admin"
        open={open}
        onClose={handleClose}
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle className="dialogTitle_admin">ADD NEW JOBTYPE</DialogTitle>
        <DialogContent className="dialogContent_admin">
          <form className="form" onSubmit={frm.handleSubmit}>
            <Grid spacing={1} container mt={1}>
              <Grid item xs={12} md={6} mt={1}>
                <TextField
                  fullWidth
                  disabled
                  id="id"
                  name="id"
                  type="text"
                  label="ID"
                  value={frm.values.id}
                  onChange={frm.handleChange}
                  onBlur={frm.handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} mt={1}>
                <TextField
                  fullWidth
                  name="tenLoaiCongViec"
                  type="text"
                  value={frm.values.tenLoaiCongViec}
                  onChange={frm.handleChange}
                  onBlur={frm.handleBlur}
                  label="JobType"
                />
              </Grid>
            </Grid>

            <DialogActions className="dialogActions_admin">
              <Button type="submit" className="btn_add">
                Add
              </Button>
              <Button onClick={handleClose} autoFocus className="btn_cancel">
                Cancel
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}
