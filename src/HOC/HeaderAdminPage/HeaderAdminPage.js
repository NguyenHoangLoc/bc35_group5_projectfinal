import React from "react";
import AdminHeader from "../../Components/AdminHeader/AdminHeader";

export default function HeaderAdminPage({ children }) {
  return (
    <div>
      <AdminHeader />
      {children}
    </div>
  );
}
