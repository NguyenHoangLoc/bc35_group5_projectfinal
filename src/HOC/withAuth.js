import { Component, useEffect } from "react";
import { userLocalService } from "../services/localService";

const withAuthen = (WrappedComponent) => {
  function WithAuthen() {
    useEffect(() => {
      if (userLocalService.get()?.role !== "ADMIN"||"admin") {
        window.location.href = "/";
      }
    }, []);
    return <WrappedComponent />;
  }
  return WithAuthen;
};
export default withAuthen;
