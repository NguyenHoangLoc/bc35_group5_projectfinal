import { useFormik } from "formik";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../../services/localService";
import { userService } from "../../services/userService";
import { withAuth } from "../../services/configURL";
import { useSelector } from "react-redux";
export default function LoginPage() {
  

  let navigate = useNavigate();
  let dispatch = useDispatch();
  let formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email")
        .required("You must to fill this section"),
      password: Yup.string().required("You must to fill this section"),
    }),
    onSubmit: (userAccount) => {
      console.log('userAccount: ', userAccount);
      userService
        .postLogin(userAccount)
        .then((res) => {
          toast.success("Login successfully");
          dispatch(setUserInfo(res.data.content));
          userLocalService.set(res.data.content);
          if(userAccount.role==="ADMIN"||"admin"){
            return navigate("/admin");
          }else{
            return navigate("/");
          }
          
        })
        .catch((err) => {
          toast.error(err.message);
          console.log(err);
        });
        
    },
  });

  return (
    <div className="flex justify-center min-h-screen text-left">
      <div className="items-center w-full max-w-3xl mx-auto p-20 lg:px-12 lg:w-3/5">
        <form action="" onSubmit={formik.handleSubmit}>
          <h1 className="uppercase text-center text-green-500 text-5xl font-bold pb-5">
            Sign In
          </h1>

          <div className="w-full">
            <label htmlFor="">
              <div className="font-bold text-3lx">Email</div>
              <input
                className=" w-96 px-2 py-3 mb-4"
                type="text"
                name="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
            </label>
          </div>
          <div>
            <label htmlFor="">
              <div className="font-bold text-3lx">Password</div>
              <input
                className="w-96 px-2 py-3 mb-4"
                type="password"
                name="password"
                onChange={formik.handleChange}
                value={formik.values.password}
              />
            </label>
          </div>
          <div>
            <button
              type="submit"
              className="uppercase w-full px-2 py-3 text-white font-bold rounded-md bg-green-500 mt-5"
            >
              Login
            </button>
          </div>
        </form>
        <div className="w-full border-t-2 mt-5 text-center">
          <h5 className="mt-3">
            Not a member yet?
            <NavLink to="/register" className="text-green-500 ml-3">
              Join now
            </NavLink>
          </h5>
        </div>
      </div>
      <div
        className="hidden bg-cover lg:block lg:w-2/5"
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1529539795054-3c162aab037a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      ></div>
    </div>
  );
}
