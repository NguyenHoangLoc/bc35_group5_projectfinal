import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { Dropdown, Space, Typography } from "antd";
import { NavLink } from "react-router-dom";
import { getWork } from "../../services/workService";
export default function JobTitle() {
  const [congViec, setCongViec] = useState([]);
  useEffect(() => {
    getWork()
      .then((res) => {
        console.log("res: ", res);
        setCongViec(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const items = [
    {
      key: "1",
      label: (
        <div>
          {congViec.map((item, index) => {
            return item.dsNhomChiTietLoai.map((item, index) => {
              return <ul> {item.tenNhom}</ul>;
            });
          })}
        </div>
      ),
    },
  ];
  const onChange = (key) => {
    console.log(key);
  };

  const renderItems = () => {
    return congViec.map((item, index) => {
      return {
        key: index,
        label: (
          <div className="text-center flex justify-center">
            <Dropdown
              menu={{
                items,
                selectable: true,
              }}
            >
              <Typography.Link>
                <Space>
                  <NavLink to={`/title/${item.id}`}>
                    {item.tenLoaiCongViec}
                  </NavLink>
                </Space>
              </Typography.Link>
            </Dropdown>
          </div>
        ),
        children: (
          <>
            <div className="  bg-green-800 flex flex-col items-center justify-center rounded-xl text-white  py-10">
              <div>
                {" "}
                <h1 className="text-2xl ">Graphics & Design</h1>
                <p className="text-xl">Designs to make you stand out.</p>
              </div>
              <div>
                {" "}
                <button className="btn btn-outline-light flex items-center text-center justify-between">
                  <svg
                    width="2rem"
                    height="2rem"
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="far"
                    data-icon="circle-play"
                    className="svg-inline--fa fa-circle-play"
                    role="img"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                    data-fa-i2svg
                  >
                    <path
                      fill="currentColor"
                      d="M188.3 147.1C195.8 142.8 205.1 142.1 212.5 147.5L356.5 235.5C363.6 239.9 368 247.6 368 256C368 264.4 363.6 272.1 356.5 276.5L212.5 364.5C205.1 369 195.8 369.2 188.3 364.9C180.7 360.7 176 352.7 176 344V167.1C176 159.3 180.7 151.3 188.3 147.1V147.1zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z"
                    />
                  </svg>
                  <span>How Fiverr Works</span>
                </button>
              </div>
            </div>
            <div className="container mt-lg-5 mt-sm-3 mb-lg-5 mb-sm-3">
              <h2 className="text-3xl ">{`Most popular in ${item.tenLoaiCongViec}`}</h2>
              <div className="flex items-center justify-between py-10">
                <div className=" px-2  items-center border rounded-lg py-2">
                  <div className="flex items-center">
                    <img
                      className="w-12 h-12 mx-1"
                      src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101614/Logo%20design_2x.png"
                      alt="..."
                    />
                    <span className="text-xl">Minimalist Logo Design</span>
                    <svg
                      height="0.8rem"
                      width="0.8rem"
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="arrow-right"
                      className="svg-inline--fa fa-arrow-right mx-1"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                      data-fa-i2svg
                    >
                      <path
                        fill="currentColor"
                        d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
                      />
                    </svg>
                  </div>
                </div>
                <div className=" px-2 items-center border rounded-lg  py-2">
                  {" "}
                  <div className="flex items-center">
                    <img
                      className="w-12 h-12 mx-1"
                      src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101618/Architecture%20_%20Interior%20Design_2x.png"
                      alt="..."
                    />
                    <span className="text-xl">
                      Architecture &amp; Interior Design
                    </span>
                    <svg
                      height="0.8rem"
                      width="0.8rem"
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="arrow-right"
                      className="svg-inline--fa fa-arrow-right mx-1"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                      data-fa-i2svg
                    >
                      <path
                        fill="currentColor"
                        d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
                      />
                    </svg>
                  </div>
                </div>
                <div className=" px-2 items-center border rounded-lg px-2 py-2">
                  <div className="flex items-center">
                    <img
                      className="w-12 h12 mx-1"
                      src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101624/Photoshop%20Editing_2x.png"
                      alt="..."
                    />
                    <span className="text-xl">Image Editing</span>
                    <svg
                      width="0.8rem"
                      hanging="0.8rem"
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="arrow-right"
                      className="svg-inline--fa fa-arrow-right mx-1"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                      data-fa-i2svg
                    >
                      <path
                        fill="currentColor"
                        d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
                      />
                    </svg>
                  </div>
                </div>
                <div className=" px-2 items-center border rounded-lg px-2 py-2">
                  <div className="flex items-center">
                    <img
                      className="w-12 h-12 mx-1"
                      src="https://fiverr-res.cloudinary.com/f_auto,q_auto/v1/attachments/generic_asset/asset/fc6c7b8c1d155625e7878252a09c4437-1653222039380/Nft%20Art%20%281%29.png"
                      alt="..."
                    />
                    <span className="text-xl">NFT Art</span>
                    <svg
                      width="0.8rem"
                      height="0.8rem"
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="arrow-right"
                      className="mx-1 svg-inline--fa fa-arrow-right"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                      data-fa-i2svg
                    >
                      <path
                        fill="currentColor"
                        d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
                      />
                    </svg>
                  </div>
                </div>
                <div className=" px-2 items-center border rounded-lg px-2 py-2">
                  <div className="flex items-center">
                    <img
                      className="w-12 h-12 mx-1"
                      src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/97477f04af40de3aa1f8d6aa21f69725-1626179101623/T-Shirts%20_%20Merchandise_2x.png"
                      alt="..."
                    />
                    <span className="text-xl">T-Shirts &amp; Merchandise</span>
                    <svg
                      width="0.8rem"
                      height="0.8rem"
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="arrow-right"
                      className="mx-1 svg-inline--fa fa-arrow-right"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                      data-fa-i2svg
                    >
                      <path
                        fill="currentColor"
                        d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"
                      />
                    </svg>
                  </div>
                </div>
              </div>
              <div>
                <div>
                  <h4 className="text-3xl">{`Export ${item.tenLoaiCongViec}`}</h4>

                  <div className="flex ">
                    {item.dsNhomChiTietLoai.map((item, index) => {
                      console.log("item1111: ", item);
                      return (
                        <div className="">
                          <img
                            className="w-40 h-40 object-cover border rounded-sm  "
                            src={item.hinhAnh}
                            alt=""
                          />
                          <h1 className="text-2xl ">{item.tenNhom}</h1>
                          {item.dsChiTietLoai.map((item, index) => {
                            return (
                              <p className="text-xl" key={index}>
                                {item.tenChiTiet}
                              </p>
                            );
                          })}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center justify-between container">
              <div className=" container mt-lg-5 mb-lg-5 mt-md-4 mb-md-4 mt-sm-2 mb-sm-2 text-center">
                <h1 className="mb-lg-5 mb-md-4 mb-sm-2 text-2xl text-inherit">
                  Services Related To Writing & Translation
                </h1>
                <div className=" flex items-center justify-center flex-wrap ">
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Minimalist logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Signature logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Mascot logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    3d logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Hand drawn logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Vintage logo design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Remove background
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Photo restoration
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Photo retouching
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Image resize
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Product label design
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2 ">
                    Custom twitch overlay
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Custom twitch emotes
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Gaming logo
                  </span>
                  <span className="border rounded-full px-2 py-1 bg-gray-200 mr-2">
                    Children book illustration
                  </span>
                </div>
              </div>
            </div>
          </>
        ),
      };
    });
  };

  return (
    <div>
      <Tabs items={renderItems()} />
    </div>
  );
}
