import { useFormik } from "formik";
import React from "react";
import DatePicker from "react-datepicker";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { boolean } from "yup";
import "react-datepicker/dist/react-datepicker.css";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../../services/localService";
import { userService } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
export default function RegisterPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { values, errors, touched, handleSubmit, handleBlur, handleChange } =
    useFormik({
      initialValues: {
        id: 0,
        name: "",
        email: "",
        password: "",
        phone: "",
        birthday: "",
        gender: boolean,
        role: "USER",
       
      },
      validationSchema:  Yup.object().shape({
        email: Yup.string()
          .required("  Email không được bỏ trống ")
          .email(" Email không đúng định dạng "),
        password: Yup.string()
          .required("  Password không được bỏ trống ")
          .min(6, " Password từ 6 - 32 ký tự ")
          .max(32, "pass từ 6 - 32 ký tự !"),
  
        name: Yup.string()
          .matches(
            /[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+$/,
            " Name Không đúng định dạng "
          )
          .required(" Name không được bỏ trống "),
        phone: Yup.string()
          .matches(
            /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
            "  Phone phải từ 03 05 07 08 09 và có 10 số "
          )
          .required(" Phone không được bỏ trống "), 
        }),
      onSubmit: (values) => {
        console.log("values: ", values);
        userService
          .postRegister(values)
          .then((res) => {
            console.log("res: ", res);
            dispatch(setUserInfo(res.data.content));
            userLocalService.set(res.data.content);
            navigate("/");

            toast.success("Sign up success!");
          })
          .catch((err) => {
            console.log(err);
            toast.error("User is exist, please choose another name");
          });
      },
    });

  return (
    <div className="flex justify-center min-h-screen text-left">
      <div
        className="hidden bg-cover lg:block lg:w-2/5"
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80)",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      ></div>

      <div className="flex items-center w-full max-w-3xl mx-auto p-8 lg:px-12 lg:w-3/5">
        <div className="w-full">
          <h1 className="text-4xl text-green-500 font-bold">REGISTER</h1>
          <p className="mt-4 text-3xl text-gray-400">
            Please register an account and join with us
          </p>
          <form className="bg-white text-left" onSubmit={handleSubmit}>
            <div className="grid grid-cols-1 gap-6 mt-8 md:grid-cols-2">
              <div>
                <label className="block mb-2 text-sm font-bold">Name</label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name}
                  placeholder="Choose your name"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
                {errors.name && touched.name ? (
                  <div className="text-danger">{errors.name}</div>
                ) : (
                  ""
                )}
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">
                  Phone number
                </label>
                <input
                  type="text"
                  name="phone"
                  id="phone"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                  placeholder="XXXX-XXX-XXX"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
                {errors.phone && touched.phone ? (
                  <div className="text-danger">{errors.phone}</div>
                ) : (
                  ""
                )}
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  placeholder="Enter your password"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
                {errors.password && touched.password ? (
                  <div className="text-danger">{errors.password}</div>
                ) : (
                  ""
                )}
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Email</label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  placeholder="please input your email"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
                {errors.email && touched.email ? (
                  <div className="text-danger">{errors.email}</div>
                ) : (
                  ""
                )}
              </div>

              <div>
                <label className="block mb-2 text-sm font-bold">Birthday</label>
                <input
                  type="date"
                  name="birthday"
                  id="birthday"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.birthday}
                  placeholder="John"
                  className="block w-full px-5 py-3 mt-2 bg-white border border-gray-400 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Gender</label>
                <div className="flex items-center">
                  <div className="flex items-center mr-6">
                    <input
                      type="checkbox"
                      name="gender"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.gender}
                      placeholder="John"
                      className="block w-full mr-1 bg-white border border-gray-400 rounded-md"
                    />
                    <span>Male</span>
                  </div>

                  <div className="flex items-center">
                    <input
                      type="checkbox"
                      name="gender"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.gender}
                      placeholder="John"
                      className="block w-full mr-1 bg-white border border-gray-400 rounded-md"
                    />
                    <span>Female</span>
                  </div>
                </div>
              </div>
            </div>
            <button
              className="flex items-center  justify-between w-full px-6 py-3 text-sm tracking-wide text-white capitalize transition-colors duration-300 transform bg-green-400 rounded-md hover:bg-green-600 mt-6"
              type="submit"
            >
              Sign Up
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
