import * as Yup from "yup";

export const registerSchema = Yup.object().shape({
 
  name: Yup.string().required("You must enter your name"),
  email: Yup
    .string()
    .email("Please enter a valid email")
    .required("You must enter an email"),
  password: Yup.string().required("You must enter a password"),
  phone: Yup.string().required("You must enter your phone number"),
  birthday: Yup.date().required("You must enter your birthday"),
  gender: Yup.boolean().required("You must choose your gender"),
  
});
