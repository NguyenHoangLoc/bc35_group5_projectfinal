import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { adminService } from "../../../services/adminService";
import { jobServiceColumns } from "../utils";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {  withAuth } from "../../../services/configURL";
import AddNewService from "../../../HOC/AddNewService/AddNewService";
import * as Yup from "yup";
import { useFormik } from "formik";
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  TextField,
} from "@mui/material";
export default function AdminServicePage() {
  const [jobServiceList, setJobServiceList] = useState([]);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [isEdit, setIsEdit] = useState(false);
  const { role } = useSelector((state) => {
    return state.userSlice.user;
  });
  withAuth(role);
  const renderJobServiceList = (jobServiceList) => {
    return jobServiceList.map((service) => {
      return {
        key: service.id,
        jobid: service.maCongViec,
        hirerid: service.maNguoiThue,
        hireday: service.ngayThue,
        condition: service.hoanThanh,
        action: (
          <div className="flex  gap-3">
            <button
              onClick={() => {
                setIsEdit(true);
                handleOpen();
                getServiceDetail(service.id);
              }}
              className=" ant-btn btn-info rounded p-2 font-semibold"
            >
              View & Edit
            </button>
            <button
              onClick={() => {
                handleDeleteService(service.id);
              }}
              className="  ant-btn btn-danger rounded p-2 font-semibold"
            >
              Delete
            </button>
          </div>
        ),
      };
    });
  };
  const handleDeleteService = (serviceID) => {
    adminService
      .deleteService(serviceID)
      .then((res) => {
        toast.success("Delete Service Successfuly ");;
        getJobServiceList();
      })
      .catch((err) => {
        toast.error("Delete Service Error");
      });
  };

  const getJobServiceList = () => {
    adminService
      .getJobServiceList()
      .then((res) => {
        setJobServiceList(renderJobServiceList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getJobServiceList();
  }, []);
  const [serviceDetail, setServiceDetail] = useState({});
  const getServiceDetail = (serviceID) => {
    adminService
      .getServiceByID(serviceID)
      .then((res) => {
        console.log("res: ", res);
        setServiceDetail(res.data.content);
      })
      .catch((err) => {
        toast.error(err.content);
      });
  };
  const [isComplete, setIsComplete] = useState(false);
  const handleChangeSatusService = (event) => {
    setIsComplete(event.target.value);
  };
  const validation = useFormik({
    enableReinitialize: true,
    initialValues: {
      maCongViec: serviceDetail.maCongViec || "",
      maNguoiThue: serviceDetail.maNguoiThue || "",
      ngayThue: serviceDetail.ngayThue || "",
      hoanThanh: serviceDetail.hoanThanh || "",
    },
    validationSchema: Yup.object({
      maCongViec: Yup.number().required("Please enter service id"),

      maNguoiThue: Yup.number().required("Please enter hirer id"),

      ngayThue: Yup.string().required("Please enter hire day"),
    }),
    onSubmit: (jobInfo) => {
      let jobInfoFinal = { ...jobInfo, hoanThanh: isComplete };
      console.log("jobInfoFinal: ", jobInfoFinal);

      if (isEdit) {
        adminService
          .putServicebyID(serviceDetail.id, jobInfoFinal)
          .then((res) => {
            toast.success("Update Service Successfully");
            getJobServiceList();
            setServiceDetail({});
          })
          .catch((err) => {
            toast.error(err.content);
          });
      }
    },
  });
  const modalCss = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",

    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
  };
  return (
    <div>
      <AddNewService />
      <Table columns={jobServiceColumns} dataSource={jobServiceList} />

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalCss}>
          <form id="user-form" onSubmit={validation.handleSubmit}>
            <div className="mb-4">
              <h1 className="text-3xl font-semibold text-center">
                UPDATE SERVICE
              </h1>
            </div>
            <div className="flex justify-start gap-2 mb-4">
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Service ID"
                  variant="outlined"
                  name="maCongViec"
                  value={validation.values.maCongViec}
                  onChange={validation.handleChange}
                />
                {validation.errors.maCongViec &&
                  validation.touched.maCongViec && (
                    <p className="text-red-400 text-sm">
                      {validation.errors.maCongViec}
                    </p>
                  )}
              </div>
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Hirer ID"
                  variant="outlined"
                  name="maNguoiThue"
                  value={validation.values.maNguoiThue}
                  onChange={validation.handleChange}
                />
                {validation.errors.maNguoiThue &&
                  validation.touched.maNguoiThue && (
                    <p className="text-red-400 text-sm">
                      {validation.errors.maNguoiThue}
                    </p>
                  )}
              </div>
            </div>
            <div
              className={`flex
               justify-start gap-2 mb-4`}
            >
              <div>
                <TextField
                  type="text"
                  id="outlined-basic"
                  label="Hire day"
                  variant="outlined"
                  name="ngayThue"
                  value={validation.values.ngayThue}
                  onChange={validation.handleChange}
                />
                {validation.errors.ngayThue && validation.touched.ngayThue && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.ngayThue}
                  </p>
                )}
              </div>
              <div>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Status</InputLabel>
                  <Select
                    sx={{ minWidth: 200 }}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={isComplete}
                    label="Status"
                    onChange={handleChangeSatusService}
                  >
                    <MenuItem value={true}>Completed</MenuItem>
                    <MenuItem value={false}>Pending</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>

            <div className="flex justify-end">
              <button
                className="px-3 py-2 text-white font-bold bg-red-400 rounded-sm "
                type="submit"
              >
                UPDATE{" "}
              </button>
            </div>
          </form>
        </Box>
      </Modal>
    </div>
  );
}
