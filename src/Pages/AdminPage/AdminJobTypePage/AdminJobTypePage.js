import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { adminService } from "../../../services/adminService";
import { jobTypeColumns } from "../utils";
import { useSelector } from "react-redux";
import {  withAuth } from "../../../services/configURL";
import AddJobType from "../../../HOC/AddNewJobType/AddNewJobType";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { useFormik } from "formik";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Grid, Input, TextField, useMediaQuery, useTheme } from "@mui/material";

export default function AdminJobTypePage() {
  const [jobTypeList, setJobTypeList] = useState([]);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const { role } = useSelector((state) => {
    return state.userSlice.user;
  });
  withAuth(role);
  const renderJobTypeList = (jobTypeList) => {
    return jobTypeList.map((jobType) => {
      return {
        id: jobType.id,
        jobtype: jobType.tenLoaiCongViec,
        action:(
          <div className="flex  gap-3">
            <button
              onClick={() => {
                setIsEdit(true);
                handleOpen();
                getJobTypeDetail(jobType.id);
                

              }}
              className=" ant-btn btn-info rounded p-2 font-semibold"
            >
              View & Edit
            </button>
            <button
              onClick={() => {
                handleDeleteJobType(jobType.id);
              }}
              className="  ant-btn btn-danger rounded p-2 font-semibold"
            >
              Delete
            </button>
          </div>

        )
      };
    });
  };
  const getJobTypeList = () => {
    adminService
      .getJobTypeList()
      .then((res) => {
        setJobTypeList(renderJobTypeList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getJobTypeList()
  }, []);
  const [jobTypeDetail, setJobTypeDetail] = useState({});
  const getJobTypeDetail = (jobTypeID) => {
    adminService
      .getJobTypeByID(jobTypeID)
      .then((res) => {
        console.log("res: ", res);
        setJobTypeDetail(res.data.content);
      })
      .catch((err) => {
        toast.error(err.content);
      });
  };
  const handleDeleteJobType = (jobTypeID) => {
    adminService
      .deleteJobType(jobTypeID)
      .then((res) => {
        toast.success("Delete Job Type Successfuly ");
        getJobTypeList();
      })
      .catch((err) => {
        toast.error("Delete Job Type Error ");
      });
  };
  const [isEdit,setIsEdit]=useState(false)
  const frm = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: jobTypeDetail.id,
      tenLoaiCongViec: jobTypeDetail.tenLoaiCongViec,
    },
    validationSchema: Yup.object({
      

      tenLoaiCongViec: Yup.string().required("Please enter job type"),
    }),
    onSubmit: (JobType) => {

      if (isEdit) {
        adminService
          .putJobTypeByID(jobTypeDetail.id, JobType)
          .then((res) => {
            toast.success("Update JobType Successfully");
            getJobTypeList();
            setJobTypeDetail({});
          })
          .catch((err) => {
            toast.error(err.content);
          });
      }
    },
  });
  return (
    <div>
      <AddJobType/>
      <Table columns={jobTypeColumns} dataSource={jobTypeList} />
      <div className="mb-3">
      <Dialog
        className="dialog_admin"
        open={open}
        onClose={handleClose}
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle className="dialogTitle_admin">UPDATE JOBTYPE</DialogTitle>
        <DialogContent className="dialogContent_admin">
          <form className="form" onSubmit={frm.handleSubmit}>
            <Grid spacing={1} container mt={1}>
              <Grid item xs={12} md={6} mt={1}>
                <TextField
                  fullWidth
                  disabled
                  id="id"
                  name="id"
                  type="text"
                  label="ID"
                  value={frm.values.id}
                  onChange={frm.handleChange}
                  onBlur={frm.handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} mt={1}>
                <TextField
                  fullWidth
                  name="tenLoaiCongViec"
                  type="text"
                  value={frm.values.tenLoaiCongViec}
                  onChange={frm.handleChange}
                  onBlur={frm.handleBlur}
                  label="JobType"
                />
              </Grid>
            </Grid>

            <DialogActions className="dialogActions_admin">
              <Button type="submit" className="btn_add">
                Save
              </Button>
              <Button onClick={handleClose} autoFocus className="btn_cancel">
                Cancel
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
    </div>
  );
}
