import React from "react";
import { Tabs } from "antd";
import AdminUserPage from "./AdminUserPage/AdminUserPage";
import AdminJobTypePage from "./AdminJobTypePage/AdminJobTypePage";
import AdminJobPage from "./AdminJobPage/AdminJobPage";
import AdminServicePage from "./AdminServicePage/AdminServicePage";

import {  withAuth } from "../../services/configURL";
import { useSelector } from "react-redux";
import withAuthen from "../../HOC/withAuth";
const AdminPage = () => {
  const { role } = useSelector((state) => {
    return state.userSlice.user;
  });
  withAuth(role);
  const items = [
    {
      key: "1",
      label: "User Management",
      children: <AdminUserPage />,
    },
    { key: "2", label: "Job Management", children: <AdminJobPage /> },
    {
      key: "3",
      label: "JobType Management",
      children: <AdminJobTypePage />,
    },
    { key: "4", label: "Service Management", children: <AdminServicePage /> },
  ];

  return (
    <div className="container">
      <Tabs tabPosition="left" items={items} />
    </div>
  );
};

export default AdminPage ;
