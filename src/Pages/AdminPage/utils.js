export const userColumns = [
  {
    title: "ID",
    dataIndex: "key",
    key: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },

  {
    title: "Role",
    dataIndex: "role",
    key: "role",
  },
  {
    title: "Certification",
    dataIndex: "certification",
    key: "certification",
  },
  {
    title: "Skill",
    dataIndex: "skill",
    key: "skill",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
  },
];

export const jobColumns = [
  {
    title: "ID",
    dataIndex: "key",
    key: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Creator",
    dataIndex: "creator",
    key: "creator",
  },
  {
    title: "Avatar",
    dataIndex: "avatar",
    key: "avatar",
  },

  {
    title: "Decription",
    dataIndex: "shortDesc",
    key: "shortDesc",
  },
  {
    title: "Price",
    dataIndex: "price",
    key: "price",
  },
  {
    title: "Rate",
    dataIndex: "rate",
    key: "rate",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
  },
];

export const jobTypeColumns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "JobType",
    dataIndex: "jobtype",
    key: "jobtype",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
  },
];
export const jobServiceColumns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Job ID",
    dataIndex: "jobid",
    key: "jobid",
  },
  {
    title: "Hirer ID",
    dataIndex: "hirerid",
    key: "hirerid",
  },
  {
    title: "Hire Day",
    dataIndex: "hireday",
    key: "hireday",
  },
  {
    title: "Condition",
    dataIndex: "condition",
    key: "condition",
    render: (text) => {
      if (text === true) {
        return "Đã Hoàn Thành";
      } else {
        return " Chưa Hoàn Thành";
      }
    },
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
  },
];
