import React, { useEffect, useState } from "react";
import { Table } from "antd";
import * as Yup from "yup";
import { adminService } from "../../../services/adminService";
import { jobColumns } from "../utils";
import { withAuth } from "../../../services/configURL";
import { Box, Modal, TextField } from "@mui/material";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import AddNewJob from "../../../HOC/AddNewJob/AddNewJob";
import { useFormik } from "formik";
export default function AdminJobPage() {
  const [jobList, setJobList] = useState([]);
  const [jobDetail, setJobDetail] = useState({});
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const { role } = useSelector((state) => {
    return state.userSlice.user;
  });
  withAuth(role);
  const getJobDetail = (jobID) => {
    adminService
      .getJobByID(jobID)
      .then((res) => {
        setJobDetail(res.data.content);
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };
  const getJobList = () => {
    adminService
      .getJobList()
      .then((res) => {
        setJobList(renderJobList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getJobList();
  }, []);
  const handleDeleteJob = (jobID) => {
    adminService
      .deleteJob(jobID)
      .then((res) => {
        toast.success(res.data.message);
        getJobList();
      })
      .catch((err) => {
        toast.error(err.response.data.content);
      });
  };
  const renderJobList = (jobList) => {
    return jobList.map((job) => {
      return {
        key: job.id,
        name: job.tenCongViec,
        avatar: <img className="w-60" src={job.hinhAnh} alt="" />,
        creator: job.nguoiTao,
        shortDesc: job.moTaNgan,
        price: job.giaTien,
        rate: job.danhGia,
        action: (
          <div className="flex  gap-3">
            <button
              onClick={() => {
                setIsEdit(true);
                handleOpen();
                getJobDetail(job.id);
              }}
              className=" ant-btn btn-danger rounded font-semibold p-2"
            >
              <span>View&Edit</span>
            </button>
            <button
              onClick={() => {
                handleDeleteJob(job.id);
              }}
              className="  ant-btn btn-info rounded font-semibold p-2"
            >
              <span>Delete</span>
            </button>
          </div>
        ),
      };
    });
  };
  const [isEdit, setIsEdit] = useState(false);
  const validation = useFormik({
    enableReinitialize: true,
    initialValues: {
      tenCongViec: jobDetail.tenCongViec || "",
      danhGia: jobDetail.danhGia || "",
      giaTien: jobDetail.giaTien || "",
      nguoiTao: jobDetail.nguoiTao || "",
      hinhAnh: jobDetail.hinhAnh || "",
      moTa: jobDetail.moTa || "",
      maChiTietLoaiCongViec: jobDetail.maChiTietLoaiCongViec || "",
      moTaNgan: jobDetail.moTaNgan || "",
      saoCongViec: jobDetail.saoCongViec || "",
    },
    validationSchema: Yup.object({
      tenCongViec: Yup.string().required("Please enter job title"),

      danhGia: Yup.number().required("Please enter job rate"),

      giaTien: Yup.number().required("Please enter job price"),

      nguoiTao: Yup.number().required("Please enter creator id"),

      hinhAnh: Yup.string().required("Please enter image link"),
      moTa: Yup.string().required("Please enter job description"),
      maChiTietLoaiCongViec: Yup.number().required(
        "Please enter job type detail id"
      ),

      moTaNgan: Yup.string().required("Please enter short description"),
      saoCongViec: Yup.number().required("Please enter job start"),
    }),
    onSubmit: (jobInfo) => {
      if (isEdit) {
        adminService
          .putJobDetail(jobDetail.id, jobInfo)
          .then((res) => {
            toast.success(res.message);
            getJobList();
            setJobDetail({});
          })
          .catch((err) => {
            toast.error(err.content);
          });
      }
    },
  });
  const modalcss = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",

    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
  };
  return (
    <div>
      <AddNewJob />
      <Table columns={jobColumns} dataSource={jobList} />
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalcss}>
          <form id="user-form" onSubmit={validation.handleSubmit}>
            <div className="mb-4">
              <h1 className="text-3xl font-bold text-center">EDIT JOB</h1>
            </div>
            <div className="flex justify-start gap-2 mb-4">
              <div>
                <TextField
                  id="outlined-basic"
                  label="Job title"
                  variant="outlined"
                  name="tenCongViec"
                  value={validation.values.tenCongViec}
                  onChange={validation.handleChange}
                />
                {validation.errors.tenCongViec &&
                  validation.touched.tenCongViec && (
                    <p className="text-red-400 text-sm">
                      {validation.errors.tenCongViec}
                    </p>
                  )}
              </div>
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Rate"
                  variant="outlined"
                  name="danhGia"
                  value={validation.values.danhGia}
                  onChange={validation.handleChange}
                />
                {validation.errors.danhGia && validation.touched.danhGia && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.danhGia}
                  </p>
                )}
              </div>
            </div>
            <div
              className={`flex
               justify-start gap-2 mb-4`}
            >
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Price"
                  variant="outlined"
                  name="giaTien"
                  value={validation.values.giaTien}
                  onChange={validation.handleChange}
                />
                {validation.errors.giaTien && validation.touched.giaTien && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.giaTien}
                  </p>
                )}
              </div>
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Creator"
                  variant="outlined"
                  name="nguoiTao"
                  value={validation.values.nguoiTao}
                  onChange={validation.handleChange}
                />
                {validation.errors.nguoiTao && validation.touched.nguoiTao && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.nguoiTao}
                  </p>
                )}
              </div>
            </div>
            <div className="flex justify-start gap-2 mb-4">
              <div>
                <TextField
                  id="outlined-basic"
                  label="Image"
                  variant="outlined"
                  name="hinhAnh"
                  value={validation.values.hinhAnh}
                  onChange={validation.handleChange}
                />
                {validation.errors.hinhAnh && validation.touched.hinhAnh && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.hinhAnh}
                  </p>
                )}
              </div>
              <div>
                <TextField
                  id="outlined-basic"
                  label="Short Description"
                  variant="outlined"
                  name="moTaNgan"
                  value={validation.values.moTaNgan}
                  onChange={validation.handleChange}
                />
                {validation.errors.moTaNgan && validation.touched.moTaNgan && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.moTaNgan}
                  </p>
                )}
              </div>
            </div>
            <div className="flex justify-start gap-2 mb-4">
              <div>
                <TextField
                  id="outlined-basic"
                  label="Description"
                  variant="outlined"
                  name="moTa"
                  value={validation.values.moTa}
                  onChange={validation.handleChange}
                />
                {validation.errors.moTa && validation.touched.moTa && (
                  <p className="text-red-400 text-sm">
                    {validation.errors.moTa}
                  </p>
                )}
              </div>
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Job type detail id"
                  variant="outlined"
                  name="maChiTietLoaiCongViec"
                  value={validation.values.maChiTietLoaiCongViec}
                  onChange={validation.handleChange}
                />
                {validation.errors.maChiTietLoaiCongViec &&
                  validation.touched.maChiTietLoaiCongViec && (
                    <p className="text-red-400 text-sm">
                      {validation.errors.maChiTietLoaiCongViec}
                    </p>
                  )}
              </div>
            </div>
            <div className="flex justify-start gap-2 mb-4">
              <div>
                <TextField
                  type="number"
                  id="outlined-basic"
                  label="Job Star"
                  variant="outlined"
                  name="saoCongViec"
                  value={validation.values.saoCongViec}
                  onChange={validation.handleChange}
                />
                {validation.errors.saoCongViec &&
                  validation.touched.saoCongViec && (
                    <p className="text-red-400 text-sm">
                      {validation.errors.saoCongViec}
                    </p>
                  )}
              </div>
            </div>
            <div className="flex justify-end">
              <button
                className="px-3 py-2 bg-red-400 text-white font-semibold rounded-sm"
                type="submit"
              >
                UPDATE{" "}
              </button>
            </div>
          </form>
        </Box>
      </Modal>
    </div>
  );
}
