import React from "react";
import Carousel from "../../Components/Carousel/Carousel";
import Footer from "../../Components/Footer/Footer";
import Marketplace from "../../Components/Marketplace/Marketplace";
import PopularServices from "../../Components/PopularServices/PopularServices";
import Selling from "../../Components/Selling/Selling";
import Testimonials from "../../Components/Testimonials/Testimonials";
import TrustedBy from "./../../Components/TrustedBy/TrustedBy";
import CategoriesMenu from "../../Components/CategoriesMenu/CategoriesMenu";
export default function HomePage() {
  return (
    <div className="">
      <CategoriesMenu />
      <Carousel />
      <TrustedBy />
      <div className="container PopularServiceCom">
        <PopularServices />
      </div>
      <Selling />
      <Testimonials />
      <Marketplace />
      
      
    </div>
  );
}
