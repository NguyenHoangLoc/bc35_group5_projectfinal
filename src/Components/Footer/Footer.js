import React from "react";
import { NavLink } from "react-router-dom";
import "./Footer.css";

export default function Footer() {
  return (
    <div>
      <div className="footer">
        {/* FOOTER MOBILE */}
        <footer className="md:hidden">
          {/* Footer Middle */}
          <div className="overflow-hidden flex flex-col lg:flex-row mx-auto p-4">
            <div className="w-full container mx-auto">
              {/* Accordions */}
              <div className="tab w-full overflow-hidden">
                <input
                  className="absolute hidden opacity-0"
                  id="tab-multi-one"
                  type="checkbox"
                  name="tabs"
                />
                <div className="label relative">
                  <label
                    className="block py-2 font-semibold tracking-wide cursor-pointer text-left text-lg"
                    htmlFor="tab-multi-one"
                  >
                    Categories
                  </label>
                  <div className="w-full absolute inset-0 flex flex-end justify-end items-center pointer-events-none">
                    <svg
                      className="icon w-8"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    ></svg>
                  </div>
                </div>
                <div className="tab-content overflow-hidden leading-normal">
                  <ul className="w-full flex flex-col text-gray-700 list-none p-0 text-left">
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Graphic & Design
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Digital Marketing
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Writing & Translation
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Video & Animation
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Music & Audio
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Programming & Tech
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Data
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Business
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Lifestyle
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Photography
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Sitemap
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="tab w-full overflow-hidden">
                <input
                  className="absolute hidden opacity-0"
                  id="tab-multi-two"
                  type="checkbox"
                  name="tabs"
                />
                <div className="label relative">
                  <label
                    className="block py-2 font-semibold tracking-wide cursor-pointer text-left text-lg"
                    htmlFor="tab-multi-two"
                  >
                    About
                  </label>
                  <div className="w-full absolute inset-0 flex flex-end justify-end items-center pointer-events-none">
                    <svg
                      className="icon w-8"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    ></svg>
                  </div>
                </div>
                <div className="tab-content overflow-hidden leading-normal">
                  <ul className="w-full flex flex-col text-gray-700 list-none p-0 text-left">
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Careers
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Press & News
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Partnerships
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Privacy Policy
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Terms of Service
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Intellectual Property Claims
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Investor Relations
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="tab w-full overflow-hidden">
                <input
                  className="absolute hidden opacity-0"
                  id="tab-multi-three"
                  type="checkbox"
                  name="tabs"
                />
                <div className="label relative">
                  <label
                    className="block py-2 font-semibold tracking-wide cursor-pointer text-left text-lg"
                    htmlFor="tab-multi-three"
                  >
                    Support
                  </label>
                  <div className="w-full absolute inset-0 flex flex-end justify-end items-center pointer-events-none">
                    <svg
                      className="icon w-8"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    ></svg>
                  </div>
                </div>
                <div className="tab-content overflow-hidden leading-normal">
                  <ul className="w-full flex flex-col text-gray-700 list-none p-0 text-left">
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Help & Support
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Trust & Safety
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Selling on Fiverr
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Buying on Fiverr
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="tab w-full overflow-hidden">
                <input
                  className="absolute hidden opacity-0"
                  id="tab-multi-four"
                  type="checkbox"
                  name="tabs"
                />
                <div className="label relative">
                  <label
                    className="block py-2 font-semibold tracking-wide cursor-pointer text-left text-lg"
                    htmlFor="tab-multi-four"
                  >
                    Community
                  </label>
                  <div className="w-full absolute inset-0 flex flex-end justify-end items-center pointer-events-none">
                    <svg
                      className="icon w-8"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    ></svg>
                  </div>
                </div>
                <div className="tab-content overflow-hidden leading-normal">
                  <ul className="w-full flex flex-col text-gray-700 list-none p-0 text-left">
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Events
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Blog
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Forum
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Community Standards
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Podcast
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Influencers
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Affiliates
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Invite a Friends
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Become a Seller
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="tab w-full overflow-hidden">
                <input
                  className="absolute hidden opacity-0"
                  id="tab-multi-five"
                  type="checkbox"
                  name="tabs"
                />
                <div className="label relative">
                  <label
                    className="block py-2 font-semibold tracking-wide cursor-pointer text-left text-lg"
                    htmlFor="tab-multi-five"
                  >
                    More From Fiverr
                  </label>
                  <div className="w-full absolute inset-0 flex flex-end justify-end items-center pointer-events-none">
                    <svg
                      className="icon w-8"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    ></svg>
                  </div>
                </div>
                <div className="tab-content overflow-hidden leading-normal">
                  <ul className="w-full flex flex-col text-gray-700 list-none p-0 text-left">
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Business
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Pro
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Logo Maker
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Guides
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Get Inspired
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Select
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        ClearVoice
                        <p className="text-gray-400">Content Marketing</p>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Fiverr Workspace
                        <p> className="text-gray-400"Invoice Software</p>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Learn
                        <p className="text-gray-400">Online Courses</p>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        to={"#"}
                        className="footer_title inline-block py-2 pr-5 font-medium text-gray-500 hover:underline"
                      >
                        Working Not Working
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          {/* Footer Bottom */}
          <div className="mx-auto px-11">
            <div className="container flex flex-col md:flex-row justify-between items-center mx-auto">
              <div className="left w-full mx-5 my-5 mb-0">
                <div className="flex justify-center">
                  <span className="fiverr-logo-footer">
                    <svg
                      width={91}
                      height={27}
                      viewBox="0 0 91 27"
                      fill="none"
                    ></svg>
                  </span>
                </div>
                <p className="text-body-2 mt-3 text-gray-400">
                  <span className="copyright text-trunc">
                    ©Fiverr International Ltd. 2023
                  </span>
                </p>
              </div>
              <div className="right mx-5 my-5 text-center">
                <ul className="social flex items-center">
                  <li className="fill-gray-600 mr-6">
                    <NavLink
                      to={"https://twitter.com/fiverr"}
                      className="social-link"
                    >
                      <span
                        className="social-icon"
                        style={{ width: 20, height: 20 }}
                        aria-hidden="true"
                      >
                        <svg width={20} height={17} viewBox="0 0 20 17"></svg>
                      </span>
                    </NavLink>
                  </li>
                  <li className="fill-gray-600 mr-6">
                    <NavLink
                      to={"https://www.facebook.com/Fiverr"}
                      className="social-link"
                    >
                      <span
                        className="social-icon"
                        style={{ width: 20, height: 20 }}
                        aria-hidden="true"
                      >
                        <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                      </span>
                    </NavLink>
                  </li>
                  <li className="fill-gray-600 mr-6">
                    <NavLink
                      to={"https://www.linkedin.com/company/fiverr-com"}
                      className="social-link"
                    >
                      <span
                        className="social-icon"
                        style={{ width: 20, height: 20 }}
                        aria-hidden="true"
                      >
                        <svg width={21} height={20} viewBox="0 0 21 20"></svg>
                      </span>
                    </NavLink>
                  </li>
                  <li className="fill-gray-600 mr-6">
                    <NavLink
                      to={"https://www.pinterest.com/fiverr"}
                      className="social-link"
                    >
                      <span
                        className="social-icon"
                        style={{ width: 20, height: 20 }}
                        aria-hidden="true"
                      >
                        <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                      </span>
                    </NavLink>
                  </li>
                  <li className="fill-gray-600">
                    <NavLink
                      to={"https://www.instagram.com/fiverr"}
                      className="social-link"
                    >
                      <span
                        className="social-icon"
                        style={{ width: 20, height: 20 }}
                        aria-hidden="true"
                      >
                        <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                      </span>
                    </NavLink>
                  </li>
                </ul>
                <section className="setting-buttons flex items-center mt-4">
                  <section className="locale-settings mr-1">
                    <button className="selection-trigger language-selection-trigger flex items-center px-3 py-2">
                      <span
                        className="trigger-icon fill-gray-600"
                        style={{ width: 16, height: 16 }}
                        aria-hidden="true"
                      >
                        <svg width={18} height={18} viewBox="0 0 18 18"></svg>
                      </span>
                      <span className="label pl-2 text-gray-500">English</span>
                    </button>
                  </section>
                  <section class="currency-settings mr-1">
                    <button class="selection-trigger currency-selection-trigger text-gray-500 px-3 py-2">
                      $ USD
                    </button>
                  </section>
                  <button
                    className="accessibility-button pl-2"
                    aria-label="Open accessibility menu"
                  >
                    <span
                      className="accessibility-icon fill-gray-600"
                      style={{ width: 32, height: 32 }}
                      aria-hidden="true"
                    >
                      <svg width={32} height={32} viewBox="0 0 32 32">
                        <circle
                          cx={16}
                          cy={16}
                          r="15.5"
                          fill="white"
                          stroke="gray"
                          className="circle-wrapper"
                        />
                      </svg>
                    </span>
                  </button>
                </section>
              </div>
            </div>
          </div>
        </footer>

        {/* FOOTER DESKTOP */}
        <footer className="hidden md:block text-white">
          <div className="container mx-auto">
            {/* Footer Middle */}
            <div className="container overflow-hidden flex flex-col lg:flex-row justify-between mx-auto p-4">
              <div className="container block md:flex text-sm mt-6 lg:mt-0">
                {/* Categories */}
                <ul className="text-gray-700 list-none p-0 font-thin flex flex-col text-left w-full">
                  <li className="footer_title inline-block py-4 px-3 text-gray-700 font-bold uppercase tracking-wide">
                    Categories
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Graphics &amp; Design
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Digital Marketing
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Writing &amp; Translation
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Video &amp; Animation
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Music &amp; Audio
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Programming &amp; Tech
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Data
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Business
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Lifestyle
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Photography
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Sitemap
                    </NavLink>
                  </li>
                </ul>

                {/* About */}
                <ul className="text-gray-700 list-none p-0 font-thin flex flex-col text-left w-full">
                  <li className="footer_title inline-block py-4 px-3 text-gray-700 font-bold uppercase tracking-wide">
                    About
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Careers
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Press &amp; News
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Partnerships
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Privacy Policy
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Terms of Service
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Intellectual Property Claims
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Investor Relations
                    </NavLink>
                  </li>
                </ul>

                {/* Support */}
                <ul className="text-gray-700 list-none p-0 font-thin flex flex-col text-left w-full">
                  <li className="footer_title inline-block py-4 px-3 text-gray-700 font-bold uppercase tracking-wide">
                    Support
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Help &amp; Support
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Trust &amp; Safety
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Selling on Fiverr
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Buying on Fiverr
                    </NavLink>
                  </li>
                </ul>

                {/* Community */}
                <ul className="text-gray-700 list-none p-0 font-thin flex flex-col text-left w-full">
                  <li className="footer_title inline-block py-4 px-3 text-gray-700 font-bold uppercase tracking-wide">
                    Community
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Events
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Blog
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Forum
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Community Standards
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Podcast
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Influencers
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Affiliates
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Invite a Friend
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Become a Seller
                    </NavLink>
                  </li>
                </ul>

                {/* More from Fiverr */}
                <ul className="text-gray-700 list-none p-0 font-thin flex flex-col text-left w-full">
                  <li className="footer_title inline-block py-4 px-3 text-gray-700 font-bold uppercase tracking-wide">
                    More From Fiverr
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Business
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Pro
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Logo Maker
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Guides
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Get Inspired
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Select
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      ClearVoice
                      <p className="text-gray-400">Content Marketing</p>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Fiverr Workspace
                      <p className="text-gray-400">Invoice Software</p>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Learn
                      <p className="text-gray-400">Online Courses</p>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to={"#"}
                      className="footer_title inline-block py-2 pl-3 pr-5 text-gray-600 text-base font-medium hover:underline"
                    >
                      Working Not Working
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>

            {/* Footer Bottom */}
            <div className="py-2 border-t-2">
              <div className="container flex flex-col md:flex-row justify-between items-center mx-auto">
                <div className="left mx-5 my-5 flex items-center">
                  <span className="fiverr-logo-footer">
                    <svg
                      width={91}
                      height={27}
                      viewBox="0 0 91 27"
                      fill="none"
                    ></svg>
                  </span>
                  <p className="text-body-2 legal ml-8 text-gray-500">
                    <span className="copyright text-trunc">
                      ©Fiverr International Ltd. 2023
                    </span>
                  </p>
                </div>

                <div className="right mx-5 my-5 flex flex-start">
                  <ul className="social flex items-center">
                    <li className="fill-gray-600 mr-6">
                      <NavLink
                        to={"https://twitter.com/fiverr"}
                        className="social-link"
                      >
                        <span
                          className="social-icon"
                          style={{ width: 20, height: 20 }}
                          aria-hidden="true"
                        >
                          <svg width={20} height={17} viewBox="0 0 20 17"></svg>
                        </span>
                      </NavLink>
                    </li>
                    <li className="fill-gray-600 mr-6">
                      <NavLink
                        to={"https://www.facebook.com/Fiverr"}
                        className="social-link"
                      >
                        <span
                          className="social-icon"
                          style={{ width: 20, height: 20 }}
                          aria-hidden="true"
                        >
                          <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                        </span>
                      </NavLink>
                    </li>
                    <li className="fill-gray-600 mr-6">
                      <NavLink
                        to={"https://www.linkedin.com/company/fiverr-com"}
                        className="social-link"
                      >
                        <span
                          className="social-icon"
                          style={{ width: 20, height: 20 }}
                          aria-hidden="true"
                        >
                          <svg width={21} height={20} viewBox="0 0 21 20"></svg>
                        </span>
                      </NavLink>
                    </li>
                    <li className="fill-gray-600 mr-6">
                      <NavLink
                        to={"https://www.pinterest.com/fiverr"}
                        className="social-link"
                      >
                        <span
                          className="social-icon"
                          style={{ width: 20, height: 20 }}
                          aria-hidden="true"
                        >
                          <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                        </span>
                      </NavLink>
                    </li>
                    <li className="fill-gray-600 mr-6">
                      <NavLink
                        to={"https://www.instagram.com/fiverr"}
                        className="social-link"
                      >
                        <span
                          className="social-icon"
                          style={{ width: 20, height: 20 }}
                          aria-hidden="true"
                        >
                          <svg width={20} height={20} viewBox="0 0 20 20"></svg>
                        </span>
                      </NavLink>
                    </li>
                  </ul>
                  <section className="setting-buttons flex items-center">
                    <section className="locale-settings mr-1">
                      <button className="selection-trigger language-selection-trigger flex items-center px-3 py-2">
                        <span
                          className="trigger-icon fill-gray-600"
                          style={{ width: 16, height: 16 }}
                          aria-hidden="true"
                        >
                          <svg width={18} height={18} viewBox="0 0 18 18"></svg>
                        </span>
                        <span className="label pl-2 text-gray-500">
                          English
                        </span>
                      </button>
                    </section>
                    <section class="currency-settings mr-1">
                      <button class="selection-trigger currency-selection-trigger text-gray-500 px-3 py-2">
                        $ USD
                      </button>
                    </section>
                    <button
                      className="accessibility-button pl-2"
                      aria-label="Open accessibility menu"
                    >
                      <span
                        className="accessibility-icon fill-gray-600"
                        style={{ width: 32, height: 32 }}
                        aria-hidden="true"
                      >
                        <svg width={32} height={32} viewBox="0 0 32 32">
                          <circle
                            cx={16}
                            cy={16}
                            r="15.5"
                            fill="white"
                            stroke="gray"
                            className="circle-wrapper"
                          />
                        </svg>
                      </span>
                    </button>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  );
}
