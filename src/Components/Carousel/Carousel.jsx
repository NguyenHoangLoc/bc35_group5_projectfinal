import React from "react";
import { Autoplay, EffectFade } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "./carousel.css";
// import Carousel from "./Carousel";
import SearchBar from "./../SearchBar/SearchBar";
export default function Carousel() {
  return (
    <div className="carousel-warpper relative ">
      {/* Carousel Background swiper */}
      <div className="carousel-backgrounds ">
        <Swiper
          spaceBetween={30}
          //   slidesPerView={1}
          effect={"fade"}
          autoplay={{
            delay: 3000,
            disableOnInteraction: false,
          }}
          modules={[EffectFade, Autoplay]}
          className="mySwiper"
          style={{ height: "90vh" }}
        >
          <SwiperSlide>
            <img
              src="https://demo5.cybersoft.edu.vn/img/1.png"
              alt="true"
              className=" w-100 object-fit "
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90vh",
              }}
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://demo5.cybersoft.edu.vn/img/2.png"
              alt="true"
              className=" w-100 object-fit"
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90vh",
              }}
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://demo5.cybersoft.edu.vn/img/3.png"
              alt="true"
              className=" w-100 object-fit"
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90vh",
              }}
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://demo5.cybersoft.edu.vn/img/4.png"
              alt="true"
              className=" w-100 object-fit"
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90vh",
              }}
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              src="https://demo5.cybersoft.edu.vn/img/5.png"
              alt="true"
              className=" w-100 object-fit"
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90vh",
              }}
            />
          </SwiperSlide>
        </Swiper>
      </div>
      {/* Carousel search */}
      <div
        className="carousel-search-bar flex items-center px-8 container mx-auto "
        style={{ height: "90vh", position: "absolute" }}
      >
        <div className="header">
          <h1 className="text-left text-white  text-5xl font-semibold mb-6">
            <span>
              Find the perfect <i> freelance</i>
              <br />
              sevices for your bussiness
            </span>
          </h1>
          <SearchBar />
          <div className="popular text-white flex items-center mt-6">
            Popular:
            <ul className="flex items-center ml-3">
              <li className="rounded-full border border-white px-3 py-0.5 mr-3">
                <a
                  href="/categories/graphics-design/website-design?source=hplo_search_tag&pos=1&name=website-design"
                  className="text-body-2"
                >
                  Website Design
                </a>
              </li>
              <li className="rounded-full border border-white px-3 py-0.5 mr-3">
                <a
                  href="/categories/programming-tech/wordpress-services?source=hplo_search_tag&pos=2&name=wordpress-services"
                  className="text-body-2"
                >
                  WordPress
                </a>
              </li>
              <li className="rounded-full border border-white px-3 py-0.5 mr-3">
                <a
                  href="/categories/graphics-design/creative-logo-design?source=hplo_search_tag&pos=3&name=creative-logo-design"
                  className="text-body-2"
                >
                  Logo Design
                </a>
              </li>
              <li className="rounded-full border border-white px-3 py-0.5 mr-3">
                <a
                  href="/cp/ai-services?source=hplo_search_tag&pos=4&name=ai-services"
                  className="text-body-2"
                >
                  AI Services
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
