import { debounce } from "lodash";
import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input } from "antd";
import { useNavigate } from "react-router-dom";
import { getDanhSachCongViecTheoTen } from "../../redux-toolkit/thunk/congViecThunk";
import DataResult from "./DataResult";
const { Search } = Input;

export default function SearchBar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [searchWord, setSearchWord] = useState("");

  const [filteredInput, setFilteredInput] = useState([]);

  const fetchDanhSachCongViec = (nextValue) => {
    dispatch(getDanhSachCongViecTheoTen(nextValue));
  };

  const debounceSearch = useRef(
    debounce((nextValue) => fetchDanhSachCongViec(nextValue), 300)
  ).current;

  const jobData = useSelector((state) => state.congViecSlice);

  const handleOnChange = (e) => {
    const { value } = e.target;
    setSearchWord(value);
    debounceSearch(value);
    const newFilter = jobData.filter((item) => {
      return item.tenChiTietLoai.toLowerCase().includes(value.toLowerCase());
    });
    setFilteredInput(newFilter);
  };

  const onSearch = () => {
    setTimeout(() => {
      navigate("/categorytype");
    }, 1000);
  };

  return (
    <div className="search-bar">
      <Search
        placeholder="Input search text"
        allowClear
        enterButton="Search"
        size="large"
        onChange={handleOnChange}
        onSearch={onSearch}
        className="search-form"
      />
      <DataResult filteredInput={filteredInput} />
    </div>
  );
}
