import { NavLink } from "react-router-dom";

import UserNav from "../UserNav/UserNav";

export default function Header() {
  return (
    <div className=" ">
      <div className="flex items-center justify-between px-24 ">
        <div className="flex items-center justify-between">
          <div>
            <NavLink className="nav-link " to="/">
              <svg
                width="89"
                height="27"
                viewBox="0 0 89 27"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g fill="#404145">
                  <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path>
                </g>
                <g fill="#1dbf73">
                  <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path>
                </g>
              </svg>
            </NavLink>
          </div>
          <div>
            <form
              className="flex items-center border rounded-lg mx-4 my-3"
              action=""
            >
              <input
                className=" px-4 py-2 text-lg "
                type="text"
                placeholder="Find Services"
              />
              <button className="bg bg-green-500 text-white text-lg px-4 py-2 ">
                Seach
              </button>
            </form>
          </div>
        </div>
        <div>
          <ul className="flex items-center justify-between text-lg ">
            <li>
              <NavLink
                className="nav-link text-blue-800 hover:text-green-500 "
                to="/"
              >
                Fiverr Business
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link text-gray-500 hover:text-green-500 "
                to="/"
              >
                Export
              </NavLink>
            </li>
            <li className="flex text-gray-500 hover:text-green-500 ">
              <svg
                className="mt-2 "
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                height="1.5rem"
                width="1.5rem"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418"
                />
              </svg>

              <NavLink
                className="nav-link text-gray-500 hover:text-green-500  "
                to="/"
              >
                English
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link text-gray-500 hover:text-green-500 "
                to="/"
              >
                US$ USD
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link text-gray-500 hover:text-green-500 "
                to="/"
              >
                Become a Seller
              </NavLink>
            </li>
            <li>
              <UserNav />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
