import React from "react";

export default function Selling() {
  return (
    <div className="container mx-auto pb-24">
      <div className="selling-wrapper">
        <div className="grid-15 flex items-center">
          <div className="col-15-xs col-7-md w-1/2 text-left pr-44">
            <h2 className="text-4xl pb-6 font-semibold text-gray-700">
              A whole world of freelance talent at your fingertips
            </h2>
            <ul style={{ listStyle: "disc" }}>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="XQskgrQ check-icon mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  ></span>
                  The best for every budget
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Find high-quality services at every price point. No hourly
                  rates, just project-based pricing.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="XQskgrQ check-icon mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  ></span>
                  Quality work done quickly
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Find the right freelancer to begin working on your project
                  within minutes.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="XQskgrQ check-icon mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  ></span>
                  Protected payments, every time
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Always know what you'll pay upfront. Your payment isn't
                  released until you approve the work.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="XQskgrQ check-icon mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  ></span>
                  24/7 support
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Questions? Our round-the-clock support team is available to
                  help anytime, anywhere.
                </p>
              </li>
            </ul>
          </div>
          <div className="col-15-xs col-8-md w-1/2">
            <div className="video-modal">
              <div className="picture-wrapper">
                <picture>
                  <source
                    media="(min-width: 1160px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 900px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 600px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_900,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_900,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 361px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(max-width: 360px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <img
                    alt="Video teaser"
                    src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png"
                    loading="auto"
                    className="w-full"
                  />
                </picture>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
