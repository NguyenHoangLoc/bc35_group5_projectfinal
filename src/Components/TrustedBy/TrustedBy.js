import React from "react";

export default function TrustedBy() {
  return (
    <div style={{ backgroundColor: "#f7efef" }}>
      <div className="flex items-center justify-center py-3">
        <span className="mr-6 font-bold text-gray-400">Trusted by:</span>
        <ul className="flex item-center justify-center space-x-10 text-base">
          <li>
            <picture>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/facebook.31d5f92.png"
                alt="facebook"
              />
            </picture>
          </li>
          <li>
            <picture>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/google.517da09.png"
                alt="Google"
              />
            </picture>
          </li>
          <li>
            <picture>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/netflix.e3ad953.png"
                alt="NETFLIX"
              />
            </picture>
          </li>
          <li>
            <picture>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/pandg.8b7310b.png"
                alt="P&G"
              />
            </picture>
          </li>
          <li className="display-from-sm">
            <picture>
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/paypal.ec56157.png"
                alt="PayPal"
              />
            </picture>
          </li>
        </ul>
      </div>
    </div>
  );
}
