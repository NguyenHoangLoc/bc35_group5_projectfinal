import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../services/localService";
import "./UserNav.css"
export default function UserNav() {
  const userDangNhap = useSelector((state) => state.userSlice.user);
  console.log("userDangNhap: ", userDangNhap);

  const handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
    window.location.href= "./"
  };



  const renderUserNav = () => {
    if (userDangNhap) {
      if (userDangNhap.role==="ADMIN"||"admin"){
        return (
          <>
            <span className="text-green-400 font-medium mr-4">
              {userDangNhap.user?.name}
            </span>
            
            <button
              className=" text-green-400 bg-transparent border border-white hover:bg-green-400 hover:border-green-400 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 logout-button"
              onClick={handleLogout}
            >
              Log out
            </button>
          </>
        )
      }else{
        return (
          <>
            <span className="text-green-400 font-medium mr-4">
              {userDangNhap?.name}
            </span>
            
            <button
              className=" text-green-400 bg-transparent border border-white hover:bg-green-400 hover:border-green-400 transition-all font-medium rounded-md text-base px-5 py-2 mr-2 logout-button"
              onClick={handleLogout}
            >
              Log out
            </button>
          </>
        );
      }
      
    } else {
      return (
        <>
          <NavLink
            to={"/login"}
            className="text-green-400 nav-info font-medium rounded-lg text-base px-4 lg:px-5 py-2 lg:py-2.5 mr-2"
          >
            Log in
          </NavLink>
          <NavLink
            to={"/register"}
            className="text-green-400 nav-info  transition-all font-medium rounded-md text-base px-5 py-2 mr-2 join-button"
          >
            Join
          </NavLink>
        </>
      );
    }
  };

  return <div>{renderUserNav()}</div>;
}
