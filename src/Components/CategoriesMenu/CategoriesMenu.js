import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { Tabs } from "antd";
import { getWork } from "../../services/workService";
import { Dropdown, Space, Typography } from "antd";
export default function CategoriesMenu(props) {
  const [congViec, setCongViec] = useState([]);
  useEffect(() => {
    getWork()
      .then((res) => {
        console.log("res: ", res);
        setCongViec(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const items = [
    {
      key: "1",
      label: (
        <div>
          {congViec.map((item, index) => {
            return item.dsNhomChiTietLoai.map((item, index) => {
              return <ul> {item.tenNhom}</ul>;
            });
          })}
        </div>
      ),
    },
  ];
  const onChange = (key) => {
    console.log(key);
  };
  const renderItems = () => {
    return congViec.map((item, index) => {
      return {
        key: index,
        label: (
          <div className="text-center flex justify-center">
            <Dropdown
              menu={{
                items,
                selectable: true,
              }}
            >
              <Typography.Link>
                <Space>
                  <NavLink to={`/title/${item.id}`}>
                    {item.tenLoaiCongViec}
                  </NavLink>
                </Space>
              </Typography.Link>
            </Dropdown>
          </div>
        ),
      };
    });
  };
  return (
    <div>
      <Tabs className="font-bold text-success" items={renderItems()} />
    </div>
  );
}
