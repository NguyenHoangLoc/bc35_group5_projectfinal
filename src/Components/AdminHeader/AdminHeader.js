import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import fiverr_logo from "../../assets/img/Font-Fiverr-Logo.jpg";
import {  withAuth } from "../../services/configURL";
import UserNav from "../UserNav/UserNav";

export default function AdminHeader() {
  let { user} = useSelector((state) => {
    return state.userSlice.user.user;
  });
withAuth(user)
  return (
    <header>
      <div className="container flex justify-between items-center py-5 border-b-2">
        <div className="ml-3 mr-3">
          <NavLink to="/">
            <img className="w-28 " src={fiverr_logo} alt="" />
          </NavLink>
        </div>
        <div className="header-right flex items-center">
          <div className="mr-3">
            <i className="fa fa-cog"></i>
          </div>
          <div className=" text-white user-avatar p-1 text-center rounded-full">
            <UserNav />
          </div>
        </div>
      </div>
    </header>
  );
}
