export const USER_LOCAL = "USER_LOCAL";

export const userLocalService = {
  get: () => {
    let userDataJson = localStorage.getItem(USER_LOCAL);
    if (userDataJson) {
      return JSON.parse(userDataJson);
    } else {
      return null;
    }
  },
  set: (userData) => {
    let userDataJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOCAL, userDataJson);
  },
  remove: () => {
    localStorage.removeItem(USER_LOCAL);
  },
};
