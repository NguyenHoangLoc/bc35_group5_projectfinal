import { https } from "./configURL";

export const userService = {
  postLogin: (userAccount) => {
    return https.post("/api/auth/signin", userAccount);
  },
  postRegister: (userAccount) => {
    return https.post("/api/auth/signup", userAccount);
  },
};
