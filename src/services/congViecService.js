import axios from "axios";
import { createConfig, DOMAIN } from "./../utils/config";

export const congViecService = {
  layDanhSachCongViecTheoTen: (tenCongViec) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${tenCongViec}`,
      method: "GET",
      data: tenCongViec,
      headers: createConfig(),
    });
  },

  layMenuLoaiCongViec: () => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-menu-loai-cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
