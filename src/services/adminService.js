import { https } from "./configURL";

export const adminService = {
  //userService
  getUserList: () => {
    return https.get("/api/users");
  },
  deleteUser: (id) => {
    return https.delete(`/api/users?id=${id}`);
  },
  postUser: (userInfo) => {
    return https.post(`/api/users`, userInfo);
  },
  putUser: (id, userInfo) => {
    return https.put(`/api/users/${id}`, userInfo);
  },
  getUser: (id) => {
    return https.get(`/api/users/${id}`);
  },
  searchUser: (user) => {
    return https.get(`/api/users/search/${user}`);
  },
  //jobService
  getJobList: () => {
    return https.get("/api/cong-viec");
  },
  getJobByID: (jobID) => {
    return https.get(`/api/cong-viec/${jobID}`);
  },
  putJobDetail: (jobID, jobDetail) => {
    return https.put(`/api/cong-viec/${jobID}`, jobDetail);
  },
  deleteJob: (jobID) => {
    return https.delete(`/api/cong-viec/${jobID}`);
  },
  addNewJob: (jobInfo) => {
    return https.post("/api/cong-viec", jobInfo);
  },
  //jobTypeService
  getJobTypeList: () => {
    return https.get("/api/loai-cong-viec");
  },
  postJobType:(jobTypeInfo)=>{
    return https.post("/api/loai-cong-viec",jobTypeInfo)
  },
  getJobTypeByID:(jobTypeID)=>{
    return https.get(`/api/loai-cong-viec/${jobTypeID}`)
  },
  putJobTypeByID: (jobTypeID, jobTypeInfo) => {
    return https.put(`/api/loai-cong-viec/${jobTypeID}`, jobTypeInfo);
  },
  deleteJobType: (jobTypeID) => {
    return https.delete(`/api/loai-cong-viec/${jobTypeID}`);
  },
  //Service
  getJobServiceList: () => {
    return https.get("api/thue-cong-viec");
  },
  getServiceByID: (serviceID) => {
    return https.get(`/api/thue-cong-viec/${serviceID}`);
  },
  putServicebyID: (serviceID, serviceInfo) => {
    return https.put(`/api/thue-cong-viec/${serviceID}`, serviceInfo);
  },
  postService: (serviceInfo) => {
    return https.post("/api/thue-cong-viec", serviceInfo);
  },
  deleteService: (serviceID) => {
    return https.delete(`/api/thue-cong-viec/${serviceID}`);
  },
};
