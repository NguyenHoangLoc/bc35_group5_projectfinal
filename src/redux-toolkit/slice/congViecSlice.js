import { createSlice } from "@reduxjs/toolkit";
import {
  getDanhSachCongViecTheoTen,
  getMenuLoaiCongViec,
} from "./../thunk/congViecThunk";

const initialState = {
  danhSachCongViecTheoTen: [],
  menuLoaiCongViec: [],
  loadingCongViec: false,
};

export const congViecSlice = createSlice({
  name: "congViecSlice",
  initialState,

  reducers: {
    layDanhSachCongViec: (state, action) => {
      state.getDanhSachCongViecTheoTen = action.payload;
    },
    layMenuCongViec: (state, action) => {
      state.menuLoaiCongViec = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(getDanhSachCongViecTheoTen.pending, (state) => {
      state.loadingCongViec = true;
      state.danhSachCongViecTheoTen = [];
    });

    builder.addCase(
      getDanhSachCongViecTheoTen.fulfilled,
      (state, { payload }) => {
        state.loadingCongViec = false;
        state.danhSachCongViecTheoTen = payload;
      }
    );

    builder.addCase(getMenuLoaiCongViec.pending, (state) => {
      state.loadingCongViec = true;
      state.menuLoaiCongViec = [];
    });

    builder.addCase(getMenuLoaiCongViec.fulfilled, (state, { payload }) => {
      state.loadingCongViec = false;
      state.menuLoaiCongViec = payload;
    });
  },
});

export const { layDanhSachCongViec, layMenuCongViec } = congViecSlice.actions;

export default congViecSlice.reducer;
